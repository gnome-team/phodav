# Basque translation for phodav.
# Copyright (C) 2019 phodav's COPYRIGHT HOLDER
# This file is distributed under the same license as the phodav package.
# Asier Sarasua Garmendia <asiersarasua@ni.eus>, 2019, 2022.
#
msgid ""
msgstr "Project-Id-Version: phodav master\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/phodav/issues\n"
"POT-Creation-Date: 2022-05-07 13:22+0000\n"
"PO-Revision-Date: 2022-06-19 10:00+0100\n"
"Last-Translator: Asier Sarasua Garmendia <asiersarasua@ni.eus>\n"
"Language-Team: Basque <librezale@librezale.eus>\n"
"Language: eu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: bin/chezdav.c:120
msgid "Print program version"
msgstr "Erakutsi programaren bertsioa"

#: bin/chezdav.c:121
msgid "Be verbose"
msgstr "Eman xehetasunak"

#: bin/chezdav.c:122
msgid "Port to listen to"
msgstr "Entzungo den ataka"

#: bin/chezdav.c:123
msgid "Listen on loopback only"
msgstr "Entzun atzera-begiztan soilik"

#: bin/chezdav.c:124
msgid "Listen on all interfaces"
msgstr "Entzun interfaze guztietan"

#: bin/chezdav.c:125
msgid "Path to export"
msgstr "Esportaziorako bide-izena"

#: bin/chezdav.c:126
msgid "Path to htdigest file"
msgstr "htdigest fitxategiaren bide-izena"

#: bin/chezdav.c:127
msgid "DIGEST realm"
msgstr "DIGEST domeinua"

#: bin/chezdav.c:128
msgid "Read-only access"
msgstr "Irakurtzeko soilik den sarbidea"

#: bin/chezdav.c:130
msgid "Skip mDNS service announcement"
msgstr "Saltatu mDNS zerbitzu-iragarkia"

#: bin/chezdav.c:142
msgid "- simple WebDAV server"
msgstr "- WebDAV zerbitzari sinplea"

#: bin/chezdav.c:143
#, c-format
msgid "Report bugs to <%s>"
msgstr "Bidali erroreei buruzko informazioa hona: <%s>"

#: bin/chezdav.c:150
#, c-format
msgid "Option parsing failed: %s\n"
msgstr "Huts egin du aukera aztertzean: %s\n"

#: bin/chezdav.c:154
#, c-format
msgid "Unsupported extra arguments: %s ...\n"
msgstr "Onartzen ez diren argumentu gehigarriak: %s …\n"

#: bin/chezdav.c:163
msgid "--local and --public are mutually exclusive\n"
msgstr "--local and --public elkarrekiko esklusiboak dira\n"

#: bin/chezdav.c:189
#, c-format
msgid "Failed to open htdigest: %s\n"
msgstr "Huts egin du htdigest irekitzeak: %s\n"

#: bin/chezdav.c:205
#, c-format
msgid "mDNS failed: %s\n"
msgstr "mDNS-k huts egin du: %s\n"

#: bin/chezdav.c:216
msgid "Internal error, should not happen\n"
msgstr "Barneko errorea, ez litzateke gertatu beharko\n"

#: bin/chezdav.c:219
#, c-format
msgid "Listen failed: %s\n"
msgstr "Entzuteak huts egin du: %s\n"
